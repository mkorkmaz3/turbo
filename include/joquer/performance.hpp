/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_PERFORMANCE_H_
#define JOQUER_PERFORMANCE_H_

#include <joquer/msr.hpp>
#include <joquer/perf_event.hpp>
#include <joquer/sysfs.hpp>
#include <joquer/thread_pool.hpp>
#include <joquer/topology.hpp>

namespace joquer {

class Performance {
public:
    typedef struct {
        uint64_t aperfStart;
        uint64_t mperfStart;
        uint64_t tscStart;
        uint64_t aperfDiff;
        uint64_t mperfDiff;
        uint64_t tscDiff;
        int      maxHwFreq;
    } FrequencyInterval;
private:
    std::shared_ptr<MSR> msr;
    std::shared_ptr<Topology> topo;
    FrequencyInterval* freqInterval;
public:
    Performance(std::shared_ptr<MSR> m = std::make_shared<MSR>(), std::shared_ptr<Topology> t = std::make_shared<Topology>()) : msr(m), topo(t) {
        CPUID::checkAperfMperf();
        if (topo->isAMD())
            SysFSAmd15h::checkPrerequisites();
        // FIXME for Intel, check Prerequisites
        int coreCount = topo->getCoreCount();
        freqInterval  = new FrequencyInterval[coreCount];
        for (int i = 0; i < coreCount; i++) {
            freqInterval[i].maxHwFreq = 1;
            freqInterval[i].mperfDiff = 1;
        }
    }

    ~Performance() { delete[] freqInterval; }

    void initInterval(int core) {
        FrequencyInterval* fi = &freqInterval[core];
        if (topo->isAMD()) {
            int pBoost = SysFSAmd15h::getPStatesBoosted();
            // CoreCOF: Core current operating frequency in MHz. CoreCOF = 100 * (MSRC001_00[6B:64][CpuFid] + 10h) / (2^MSRC001_00[6B:64][CpuDid]).
            fi->maxHwFreq = (msr->getFid(core, pBoost) + 0x10) * 200000 / (2 << msr->getDid(core, pBoost)); // kHz in SW P0
        } else {
            fi->maxHwFreq = msr->getPStateFast(0) * 100;
        }
        fi->tscStart   = Hardware::readTSC();
        fi->aperfStart = msr->getAPerf(core);
        fi->mperfStart = msr->getMPerf(core);
    }

    void initInterval() { initInterval(Hardware::getCoreId()); }

    void splitInterval(int core) __attribute__((optimize(0))) {
        FrequencyInterval* fi = &freqInterval[core];
        uint64_t aperf = msr->getAPerf(core);
        uint64_t mperf = msr->getMPerf(core);
        uint64_t tsc   = Hardware::readTSC();

        fi->aperfDiff  = aperf - fi->aperfStart;
        fi->aperfStart = aperf;
        fi->mperfDiff  = mperf - fi->mperfStart;
        fi->mperfStart = mperf;
        fi->tscDiff    = tsc - fi->tscStart;
        fi->tscStart   = tsc;
    }

    void splitInterval() { splitInterval(Hardware::getCoreId()); }

    uint64_t getIntervalFrequency(int core) {
        FrequencyInterval* fi = &freqInterval[core];
        return (fi->aperfDiff * fi->maxHwFreq) / fi->mperfDiff; //KHz
    }

    uint64_t getIntervalFrequency() { return getIntervalFrequency(Hardware::getCoreId()); }

    uint64_t getIntervalTimeC0(int core) {
        FrequencyInterval* fi = &freqInterval[core];
        return (fi->mperfDiff > fi->tscDiff) ? (fi->tscDiff / fi->maxHwFreq) : (fi->mperfDiff / fi->maxHwFreq); //ms
    }

    uint64_t getIntervalTimeC0() { return getIntervalTimeC0(Hardware::getCoreId()); }

    uint64_t getIntervalTimeCx(int core) {
        FrequencyInterval* fi = &freqInterval[core];
        return (fi->mperfDiff > fi->tscDiff) ? 0 : (fi->tscDiff - fi->mperfDiff) / fi->maxHwFreq; //ms
    }

    uint64_t getIntervalTimeCx() { return getIntervalTimeCx(Hardware::getCoreId()); } 
    
    void printPerformance(int core) {
        std::cout << std::setw(3) << core << std::setw(7) << msr->getPState(core) << std::setw(4) << msr->getVid(core) << std::setw(4) << msr->getFid(core) << std::setw(4) << msr->getDid(core) << std::setprecision(3) << std::setw(5) << (getIntervalFrequency(core) / 1000000.0) << std::setw(6) << getIntervalTimeC0(core) << std::setw(6) << getIntervalTimeCx(core) << std::endl;
    }
    
    void printPerformance() {
        std::cout << "CPU Pstate Vid Fid Did Freq    C0    Cx" << std::endl;

        int coreCount = topo->getCoreCount();
        freqInterval  = new FrequencyInterval[coreCount];
        for (int i = 0; i < coreCount; i++)
            printPerformance(i);
    }

    std::string printCpuInfoHeader() { return std::string(" Pstate Vid Fid Did Freq PmaxSW"); }
    std::string printAMDHeader() { return std::string(" Pboost PmaxHW PlimHW"); }

    std::string printCpuInfo(int core) {
        int fid = msr->getFid(core);
        int did = msr->getDid(core);
        std::stringstream i;
        i << std::setw(7) << msr->getPState(core) << std::setw(4) << msr->getVid(core) << std::setw(4) << fid << std::setw(4) << did;
        i << std::setprecision(3) << std::setw(5) << (fid + 16) * 200000 / (2 << did) / 1000000.0 << std::setw(7) << msr->getPStateSlow(core);
        return i.str();
    }

    std::string printAMDInfo() {
        std::stringstream i;
        i << std::setw(7) << SysFSAmd15h::getPStatesBoosted() << std::setw(7) << SysFSAmd15h::getPStatesHwMaxVal() << std::setw(7) << SysFSAmd15h::getPStatesSwMaxVal();
        return i.str();
    }

    void printCpuInfos(std::vector<int> cores = std::vector<int>()) {
        if (cores.empty())
            cores = SysFSBase::getCoresOnline(); // FIXME use Topology

        std::cout << "CPU" << printCpuInfoHeader() << printAMDHeader() << std::endl; // FIXME for Intel
        for(std::vector<int>::iterator it = cores.begin(); it != cores.end(); it++)
            std::cout << std::setw(3) << *it << printCpuInfo(*it) << printAMDInfo() << std::endl; // FIXME for Intel

    }

    void printCpuInfos(int times = 1, std::string cpus = "all") {
        std::vector<int> cores;

        if (cpus == "all")
            cores = SysFSBase::getCoresOnline(); // FIXME use Topology
        else
            cores = Helper::parseSequence(cpus);

        std::stringstream r;
        for (unsigned int i = 0; i < cores.size() + 1; i++)
            r << "\e[A";

        for (int i = 0; i < times - 1; i++) {
            printCpuInfos(cores);
            std::cout << r.str();
        }
        printCpuInfos(cores);

        std::cout << "MSR NbPstateDis " << msr->getNbPstateDis(0) << " SwNbPstateLoDis " << SysFSAmd15h::getSwNbPstateLoDis() << " NbPstateDisOnP0 " << SysFSAmd15h::getNbPstateDisOnP0() << " VSRampSlamTime " << SysFSAmd15h::getVSRampSlamTime() << " PllLockTime " << SysFSAmd15h::getPllLockTime() << " SviHighFreqSel " << SysFSAmd15h::getSviHighFreqSel() << std::endl; // FIXME for Intel
    }
};

}

#endif /* JOQUER_PERFORMANCE_H_ */

