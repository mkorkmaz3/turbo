/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_THREAD_STORAGE_H_
#define JOQUER_THREAD_STORAGE_H_

#include <atomic>

#include <joquer/debug.hpp>

namespace joquer {

class ThreadStorageSingleton {
    static const unsigned int maxKey = 100;
public:
    static unsigned int getKey() {
        static std::atomic<unsigned int> joquerTLSkey(0);
        unsigned int key = joquerTLSkey++;
        if (key >= maxKey)
            Debug::userError("Too many ThreadStorage variables");
        return key;
    }
    static void** getData(unsigned int key) {
        static __thread void* data[maxKey];
        return &data[key];
    }
};

template<typename T>
class ThreadStorage {
private:
    unsigned int key;
public:
    ThreadStorage() {
        key = ThreadStorageSingleton::getKey();
        set(nullptr);
    }
    ~ThreadStorage() {}

    T* get() { return static_cast<T*>(*ThreadStorageSingleton::getData(key)); }
    void set(T* value) { *ThreadStorageSingleton::getData(key) = static_cast<void*>(value); }
};

}

#endif /* JOQUER_THREAD_STORAGE_H_ */
