/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_CPUID_H_
#define JOQUER_CPUID_H_

#include <algorithm>
#include <vector>

#include <joquer/debug.hpp>
#include <joquer/hardware.hpp>

namespace joquer {

class CPUID {
    static bool checkVendor(const char* vendor) {
        unsigned int eax, ebx, ecx, edx;
        
        Hardware::cpuid(0, &eax, &ebx, &ecx, &edx);
        if (strncmp(vendor + 0, (const char*) &ebx, 4) || strncmp(vendor + 4, (const char*) &edx, 4) || strncmp(vendor + 8, (const char*) &ecx, 4))
           return false;
        else
            return true;
    }

    static bool checkHypervisor() {
        unsigned int eax, ebx, ecx, edx;

        Hardware::cpuid(1, &eax, &ebx, &ecx, &edx);
        if (ecx >> 31)
            return true;
        else
            return false;
    }

    static bool checkIntelModel(std::vector<int> models) {
        unsigned int eax, ebx, ecx, edx;

        Hardware::cpuid(1, &eax, &ebx, &ecx, &edx);
        if (std::find(models.begin(), models.end(), ((eax >> 12) & 0xF0) + ((eax >> 4) & 0xF)) == models.end()) // (Extended_Model_ID « 4) + Model_ID
            return false;
        else
            return true;
    }
public:
    static bool checkAperfMperf() {
        unsigned int eax, ebx, ecx, edx;

        Hardware::cpuid(0, &eax, &ebx, &ecx, &edx);
        if (eax < 6)
            Debug::userError("CPUID:checkAperfMperf:Largest standard function too small");

        Hardware::cpuid(6, &eax, &ebx, &ecx, &edx);    
        if (!(ecx & 0x1))
            Debug::userError("No aperf or mperf available");

        return true;
    }
    
    static bool checkAMDFamily15h(bool abort = false) {
        unsigned int eax, ebx, ecx, edx;

        if (!checkVendor("AuthenticAMD")) {
            if (abort)
                Debug::userError("CPUID:checkAMDFamily15h:Not an AMD CPU");
            else
                return false;
        }

        if (checkHypervisor()) {
            if (abort)
                Debug::userError("CPUID:checkAMDFamily15h:Running virtualized");
            else
                return false;
        }

        Hardware::cpuid(1, &eax, &ebx, &ecx, &edx);
        if (((eax >> 8) & 0x0F) + ((eax >> 20) & 0xFF) != 0x15) {
            if (abort)
                Debug::userError("CPUID:checkAMDFamily15h:Not a family 15h CPU");
            else
                return false;
        }

        return true;
    }

    static bool checkIntelFamily6(bool abort = false) {
        unsigned int eax, ebx, ecx, edx;

        if (!checkVendor("GenuineIntel")) {
            if (abort)
                Debug::userError("CPUID:checkIntelHaswell:Not an Intel CPU");
            else
                return false;
        }

        if (checkHypervisor()) {
            if (abort)
                Debug::userError("CPUID:checkIntelHaswell:Running virtualized");
            else
                return false;
        }
        
        Hardware::cpuid(1, &eax, &ebx, &ecx, &edx);
        if (((eax >> 8) & 0xF) != 0x6) {
            if (abort)
                Debug::userError("CPUID:checkIntelHaswell:Not a family 6 CPU");
            else
                return false;
        }

        return true;
    }

    /* Family 6 Models
     * 0x2c              Westmere Xeon 5600
     * 0x2a, 0x2d        Sandy Bridge
     * 0x3e              Ivy Bridge Xeon
     * 0x3a              Ivy Bridge
     * 0x3c, 0x45, 0x46  Haswell
     * 0x3f              Future Xeon
     */

    static bool isIntelSandyBridge() { return checkIntelModel({0x2a, 0x2c, 0x2d, 0x3e}); } // includes Westmere Xeon and Ivy Bridge Xeon

    static bool isIntelIvyBridge() { return checkIntelModel({0x3a}); }

    static bool isIntelHaswell() { return checkIntelModel({0x3c, 0x3f, 0x45, 0x46}); } // includes Future Xeon

    static void checkOpportunisticPerformance() { // Intel
        unsigned int eax, ebx, ecx, edx;
        Hardware::cpuid(0x06, &eax, &ebx, &ecx, &edx);
        if (!(eax & 0x01))
            Debug::userError("CPUID:checkOpportunisticPerformance:opportunistic processor performance not enabled by BIOS");
    }

    static void getCorePerformanceBoost() { // AMD
        unsigned int eax, ebx, ecx, edx;
        Hardware::cpuid(0x80000007, &eax, &ebx, &ecx, &edx);
        if ((edx & (1U << 7)) == 0)
            Debug::userError("CPUID:getCorePerformanceBoost:not supported");
    }

    static int getL2CacheLineSize() {
        unsigned int eax, ebx, ecx, edx;
        Hardware::cpuid(0x80000006, &eax, &ebx, &ecx, &edx);
        return ecx & 0xFF;
    }

    static bool getMonitorFeature() {
        unsigned int eax, ebx, ecx, edx;
        Hardware::cpuid(0x01, &eax, &ebx, &ecx, &edx);
        return ecx & 0x8;
    }
};

}

#endif /* JOQUER_CPUID_H_ */
