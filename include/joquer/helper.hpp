/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_HELPER_H_
#define JOQUER_HELPER_H_

#include <algorithm>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <stdint.h>
#include <assert.h>

namespace joquer {

class Helper {
public:
    static uint32_t getBits(uint32_t word, int from, int till) {
        assert(till >= from);
        uint64_t mask = ((1ULL << (till + 1 - from)) - 1) << from;
        return (word & mask) >> from;
    }

    static uint32_t setBits(uint32_t word, int from, int till, uint32_t val) {
        assert(till >= from);
        uint64_t mask = ((1ULL << (till + 1 - from)) - 1) << from;
        return (word & ~mask) | ((val << from) & mask);
    }

    static std::vector<int> parseSequence(const std::string seq) {
        std::stringstream content(seq);
        std::vector<int> list;
        std::string part;

        while (std::getline(content, part, ','))
        {
            size_t pos = part.find('-');
            if (pos != std::string::npos)
            {
                std::stringstream left(part.substr(0, pos));
                std::stringstream right(part.substr(pos + 1, part.size()));

                int l, r;
                left >> l;
                right >> r;

                for (int i = l; i <= r; i++)
                    list.push_back(i);
            } else {
                std::stringstream convert(part);
                int i;
                convert >> i;
                list.push_back(i);
            }
        }

        return list;
    }

    static std::vector<std::string> parseList(const std::string li) {
        std::stringstream content(li);
        std::vector<std::string> list;
        std::string item;

        while(std::getline(content, item, ' '))
            if (content.good())
                list.push_back(item);

        return list;
    }

    template <typename T = std::string> static std::string vectorToString(const std::vector<T> &v, const std::string delimiter = " ") {
        assert(!v.empty());
        std::stringstream ss;
        std::copy(v.begin(), v.end(), std::ostream_iterator<T>(ss, delimiter.c_str()));
        std::string res = ss.str();
        return res.erase(res.length() - delimiter.length());
    }

    template <typename T> static std::string numberToString(const T& t) {
        std::stringstream ss;
        ss << t;
        return ss.str();
    }

    template <typename T> static T stringToNumber(const std::string& s) {
        std::stringstream ss(s);
        T t;
        ss >> t;
        return t;
    }
};

}

#endif /* JOQUER_HELPER_H_ */
