/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_HARDWARE_H_
#define JOQUER_HARDWARE_H_

namespace joquer {

#if !defined(__GNUC__)
# error No support for compilers other than GCC
#endif

#if !defined(__x86_64__) && !defined(__i386__)
# error No support for architectures other than i386 and x86_64
#endif

class Hardware {
public:
    static inline void cpuid(unsigned int func, unsigned int *eax, unsigned int *ebx, unsigned int *ecx, unsigned int *edx) {
        *eax = func;
        *ecx = 0;
      
        asm volatile(
#if defined(__i386__) && defined(__PIC__)
                "push %%ebx\n"
                "cpuid\n"
                "movl %%ebx, %1\n"
                "pop %%ebx\n"
#else
                "cpuid\n"
#endif
                : "=a" (*eax),
#if defined(__i386__) && defined(__PIC__)
                "=r" (*ebx),
#else
                "=b" (*ebx),
#endif
                "=c" (*ecx),
                "=d" (*edx)
                : "0" (*eax), "2" (*ecx));
    }

    static inline void pause() { asm volatile("pause"); }

    static inline void monitor(const void* address, uint32_t extensions = 0, uint32_t hints = 0) { asm volatile("monitor" :: "a" (address), "c" (extensions), "d"(hints)); }

    static inline void mwait(uint32_t hints = 0, uint32_t extensions = 1) { asm volatile("mwait" :: "a" (hints), "c" (extensions)); }

    static inline uint64_t readTSC(int* coreId = nullptr) {
#if defined(__x86_64__)
        uint64_t hi, lo, aux;
        asm volatile("rdtscp" : "=a"(lo), "=d"(hi), "=c"(aux));
        if (coreId != nullptr)
            *coreId = aux;
        return (uint64_t)hi << 32 | lo;
#elif defined(__i386__)
        uint64_t x;
        asm volatile(".byte 0x0f, 0x31" : "=A" (x));
        if (coreId != nullptr)
            Debug::userError("Hardware:readTSC:coreId not available");
        return x;
#endif
    }

    static inline int getCoreId() {
        int coreId;
        readTSC(&coreId);
        //int rc = syscall(__NR_getcpu, &coreId, nullptr, nullptr);
        //Debug::checkErrorCode("Hardware:getCoreId:getcpu", rc);
        return coreId;
    }

    static inline void illegallInstruction() {asm volatile("ud2a"); }

    static void compilerBarrier() { asm volatile("":::"memory"); }
};

}

#endif /* JOQUER_HARDWARE_H_ */
