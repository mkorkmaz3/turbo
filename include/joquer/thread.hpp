/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_THREAD_H_
#define JOQUER_THREAD_H_

#include <csignal>
#include <pthread.h>
#ifndef __USE_GNU
# define __USE_GNU
#endif
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif
#include <sched.h>
#include <sys/syscall.h>
//#include <thread>
#include <joquer/debug.hpp>
#include <joquer/sysfs.hpp>
#include <joquer/hardware.hpp>

namespace joquer {

class Thread {
public:
    typedef void (*workFunc)(void* arg, Thread* t);
private:
    //std::thread* thread;
    int threadId;
    int coreId;
    pthread_t pthreadId;
    pid_t tid;
    workFunc workFptr;
    void* workArg;

    static void* execute(void* arg) {
        Thread* thread = static_cast<Thread*>(arg);
        //thread->pthreadId = pthread_self();
        thread->coreId = sched_getcpu();
        thread->tid = syscall(SYS_gettid);

        if (thread->workFptr != nullptr)
            thread->workFptr(thread->workArg, thread);

        return nullptr;
    }

    Thread(Thread& copy);
public:
    Thread(int id) : threadId(id), coreId(-1), pthreadId(0), tid(0), workFptr(nullptr) {}

    void create(workFunc func, void* arg = nullptr) {
        workFptr = func;
        workArg = arg;
        // thread = new std::thread(Thread::execute, this); must delete in destructor
        int rc = pthread_create(&pthreadId, nullptr, Thread::execute, static_cast<void*>(this));
        Debug::checkReturnCode("Thread:create", rc);
    }

    void record(Thread::workFunc func = nullptr, void* arg = 0) {
        // thread = new std::thread(); C++11 threads cannot be assigned an existing native handle
        pthreadId = pthread_self();
        coreId = sched_getcpu(); // syscall(__NR_getcpu, &coreId, NULL, NULL);
        tid = syscall(SYS_gettid);
        workFptr = func;
        workArg = arg;
        if (workFptr != nullptr)
            workFptr(workArg, this);
    }

    void detach() {
        // thread->detach();
        if (pthreadId != 0) {
            int rc = pthread_detach(pthreadId);
            Debug::checkReturnCode("Thread:detach", rc);
        }
    }

    void join() {
        // if (thread->joinable()) thread->join();
        if (pthreadId != 0) {
            int rc = pthread_join(pthreadId, nullptr);
            Debug::checkReturnCode("Thread:join", rc);
            pthreadId = 0;
        }
    }

    void exit() {
        if(pthread_self() == pthreadId) {
            pthread_exit(nullptr);
            pthreadId = 0;
        } else if (pthreadId != 0) {
            int rc = pthread_cancel(pthreadId); // pthread_kill(pthreadId, SIGKILL);
            Debug::checkReturnCode("Thread:exit", rc);
            pthreadId = 0;
        }
    }

    void sleep(unsigned int duration) { //us
        struct timespec interval;
        interval.tv_sec = (duration * 1000) / 1000000000;
        interval.tv_nsec = (duration * 1000) % 1000000000;
        int rc;
        while ((rc = nanosleep(&interval, &interval)) == -1 && errno == EINTR) {}
        Debug::checkErrorCode("Thread:sleep:nanosleep", rc);
    }

    void migrate(int core) {
        coreId = core;
        cpu_set_t set;
        CPU_ZERO(&set);
        CPU_SET(coreId, &set);
        int rc = pthread_setaffinity_np(pthreadId, sizeof(set), &set); 
        Debug::checkReturnCode("Thread:migrate:setaffinity", rc);
    }

    int getThreadId() { return threadId; }

    int getCoreId() { return coreId; }

    pthread_t getPThreadHandle() { return pthreadId; }

    pid_t getThreadHandle() { return tid; }

    workFunc getWorkFptr() { return workFptr; }
};

}

#endif /* JOQUER_THREAD_H_ */
