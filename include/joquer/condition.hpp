/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_CONDITION_H_
#define JOQUER_CONDITION_H_

#include <ctime>
#include <joquer/debug.hpp>
#include <joquer/mutex.hpp>

namespace joquer {

class Condition {
private:
    pthread_cond_t condition;
    Mutex* mutex;
public:
    Condition(Mutex* mutex = nullptr) {
        this->mutex = mutex;
        int rc = pthread_cond_init(&condition, nullptr);
        Debug::checkReturnCode("Condition:Condition", rc);
    }

    ~Condition() {
        int rc = pthread_cond_destroy(&condition);
        Debug::checkReturnCode("Condition:Condition", rc);
    }

    void wait(Mutex* mutex = nullptr) {
        if (mutex == nullptr) {
            if (this->mutex == nullptr)
                Debug::userError("Condition:wait mutex nullptr");
            else
                mutex = this->mutex;
        }
        assert(this->mutex != nullptr);
        int rc = pthread_cond_wait(&condition, mutex->getHandle());
        Debug::checkReturnCode("Condition:wait", rc);
    }

    void signal() {
        int rc = pthread_cond_signal(&condition);
        Debug::checkReturnCode("Condition:notifyOne", rc);
    }

    void broadcast() {
        int rc = pthread_cond_broadcast(&condition);
        Debug::checkReturnCode("Condition:notifyAll", rc);
    }

    bool timedWait(long msTimeout, Mutex* mutex = nullptr) {
        if (mutex == nullptr)
            mutex = this->mutex;
        assert(mutex != nullptr);

        struct timespec timeout;
        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += msTimeout / 1000;
        timeout.tv_nsec += (msTimeout % 1000) * 1000000;

        int rc = pthread_cond_timedwait(&condition, mutex->getHandle(), &timeout);
        if (rc == ETIMEDOUT)
            return false;
        else
            return Debug::checkReturnCode("Condition:waitTimeout", rc);
    }

    pthread_cond_t* getHandle() { return &condition; }
};

}

#endif /* CONDITION_H_ */
