/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_PERF_EVENT_H_
#define JOQUER_PERF_EVENT_H_

#include <asm/unistd.h>
#include <linux/perf_event.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <joquer/debug.hpp>

#define PERF_COUNT_RAW_MAP_WAIT_CYCLES = 0x0068

namespace joquer {

// http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/include/linux/perf_event.h
// http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/include/uapi/linux/perf_event.h
// http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/kernel/events/core.c

class PerfEvent {
    int fd;

    static int openPerfEvent(pid_t tid, int core, uint64_t eventConfig, uint32_t eventType = PERF_TYPE_HARDWARE) {
        // http://www.eece.maine.edu/~vweaver/projects/perf_events/perf_event_open.html
        // http://perfmon2.sourceforge.net/manv4/pfm_get_os_event_encoding.html
        struct perf_event_attr pe;
        memset(&pe, 0, sizeof(struct perf_event_attr));
        pe.type = eventType;
        pe.size = sizeof(struct perf_event_attr);
        pe.config = eventConfig;
        pe.exclude_kernel = 1;
        pe.exclude_hv = 1;
        pe.disabled = 1;

        int fd = syscall(__NR_perf_event_open, &pe, 0, core, -1, 0);
        Debug::checkErrorCode("PerfEvent:open:syscall", fd);
        return fd;
    }
    
public:
    PerfEvent(int core, pid_t tid = 0, uint64_t eventConfig = PERF_COUNT_HW_INSTRUCTIONS, uint32_t eventType = PERF_TYPE_HARDWARE) {
        fd = openPerfEvent(tid, core, eventConfig, eventType);

        int rc = ioctl(fd, PERF_EVENT_IOC_RESET, 0);
        Debug::checkErrorCode("PerfEvent:Constructor:ioctl:reset", rc);

        rc = ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
        Debug::checkErrorCode("PerfEvent:Constructor:ioctl:enable", rc);
    }

    ~PerfEvent() {
        int rc = ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
        Debug::checkErrorCode("PerfEvent:Destructor:ioctl:disable", rc);

        close(fd);
        fd = -1;
    }

    uint64_t getCounter() {
        uint64_t count;

        int bytes = read(fd, &count, sizeof(count));
        Debug::checkErrorCode("PerfEvent:getCounter:read1", bytes);
        Debug::checkReturnCode("PerfEvent:getCounter:read2", bytes, sizeof(count));

        return count;
    }

    void resetCounter() {
        int rc = ioctl(fd, PERF_EVENT_IOC_RESET, 0);
        Debug::checkErrorCode("PerfEvent:resetCounter:ioctl", rc);
    }
};

}

#endif /* JOQUER_PERF_EVENT_H_ */
