/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_P_STATES_H_
#define JOQUER_P_STATES_H_

#include <cmath>
#include <cstddef>
#include <memory>

#include <joquer/cpuid.hpp>
#include <joquer/msr.hpp>
#include <joquer/sysfs.hpp>
#include <joquer/thread_pool.hpp>

namespace joquer {

class PStates {
    struct PStatus {
        int curr;
        int fast;
        int base;
        int slow;
        int pad[12]; // FIXME alignas
    };
    
    PStatus* pStatus;
    std::shared_ptr<Topology> topo;
    std::shared_ptr<MSR> msr;
    bool boostingEnabled;
    int pAuto;

    int readPState(int core) {
        pStatus[core].curr = msr->getPState(core) + pAuto;
        return pStatus[core].curr;
    }

    int readPState() {
        int core = Hardware::getCoreId();
        pStatus[core].curr = msr->getPState() + pAuto;
        return pStatus[core].curr;
    }

    bool checkBoosting() {
        if (SysFSCpuFreq::governorAvailable() || SysFSIntelPstate::governorAvailable()) 
            Debug::userError("PStates:checkBoosting:Governor active");

        if (topo->isAMD()) {
            CPUID::getCorePerformanceBoost();

            if (SysFSAmd15h::getPStatesBoostSrc() != 1)
                return false;

            if (pAuto ^ SysFSAmd15h::getPStatesMasterEnable())
                return false;
        } else if (topo->isIntel())
            CPUID::checkOpportunisticPerformance();
        
        for (auto p : topo->getPackages())
            if (!msr->getBoostingPackage(p))
                return false;

        for (auto c : topo->getCoresOnline())
            if (!msr->getBoostingCore(c))
                return false;

        return true;
    }

    /*template <typename T>
    T* aligned_alloc(std::size_t num = 1, std::size_t alignment = alignof(T)) { // works for Clang 3.3 / GCC 4.8
        assert(alignment <= alignof(max_align_t));
        std::size_t space = num * sizeof(T) + alignment - alignof(max_align_t);
        void* ptr = malloc(space); // FIXME use new T or better unique_ptr
        Debug::checkErrorCode("PStates:aligned_alloc:malloc", *reinterpret_cast<int*>(&ptr), 0);
        void* ret = std::align(alignment, sizeof(T), ptr, space);
        Debug::checkErrorCode("PStates:aligned_alloc:align", *reinterpret_cast<int*>(&ret), 0);
        return reinterpret_cast<T*>(ret);
    }*/
public:
    PStates(std::shared_ptr<Topology> t = std::make_shared<Topology>(), std::shared_ptr<MSR> m = std::make_shared<MSR>()) : topo(t), msr(m) {
        int ret = posix_memalign((void**)&pStatus, CPUID::getL2CacheLineSize(), sizeof(PStatus) * topo->getCoreCount());
        Debug::checkReturnCode("PStates:PStates:posix_memalign", ret);
        // FIXME pStatus = aligned_alloc<PStatus>(topo->getCoreCount(), CPUID::getL2CacheLineSize());

        boostingEnabled = checkBoosting();
        pAuto = topo->isAMD() ? SysFSAmd15h::getPStatesBoosted() : 0;

        for (auto i : topo->getCoresOnline()) {
            pStatus[i].curr = msr->getPState(i);
            pStatus[i].fast = msr->getPStateFast(i);
            pStatus[i].base = msr->getPStateBase(i) + pAuto;
            pStatus[i].slow = msr->getPStateSlow(i) + pAuto;
        }
    }

    int getPState(int core) { return pStatus[core].curr; }

    int getPState() { return getPState(Hardware::getCoreId()); }

    void setPStateFast(int pState, int core = -1) {
        msr->setPState(pState - pAuto, -1, topo->isIntel() && !boostingEnabled);
        pStatus[core == -1 ? Hardware::getCoreId() : core].curr = pState;
    }

    void setPState(int pState, int core) {
        if ((topo->isIntel() && (pState > pStatus[core].fast || pState < pStatus[core].slow)) || 
            (topo->isAMD() && (pState < pStatus[core].fast || pState > pStatus[core].slow)) ) {
            std::stringstream msg;
            msg << "PStates:setPState:outOfRange " << pStatus[core].fast << "-" << pStatus[core].slow << " requested " << pState;
            Debug::userError(msg.str().c_str());
        }

        msr->setPState(pState - pAuto, core, topo->isIntel() && !boostingEnabled);
        pStatus[core].curr = pState;
    }

    void setPState(int pstate, std::string cpus) {
        std::vector<int> cores;

        if (cpus == "all")
            cores = topo->getCoresOnline();
        else
            cores = Helper::parseSequence(cpus);

        for (auto i : cores)
            setPState(pstate, i);
    }

    void setPerformance(int pct) { // 0% = slow, 100% = boostingEnabled ? fast : base
        assert(pct >= 0 && pct <= 100);

        int core = Hardware::getCoreId();
        int pState = 0;

        if (topo->isAMD())
            pState = pStatus[core].slow - (((boostingEnabled ? pStatus[core].slow - pStatus[core].fast : pStatus[core].slow - pStatus[core].base) * pct) / 100);

        if (topo->isIntel())
            pState = (((boostingEnabled ? pStatus[core].fast - pStatus[core].slow : pStatus[core].base - pStatus[core].slow) * pct) / 100) + pStatus[core].slow;

        setPStateFast(pState, core);
    }

    void setPerformanceSlow() {
        int core = Hardware::getCoreId();
        setPStateFast(pStatus[core].slow, core);
    }

    void setPerformanceBase() {
        int core = Hardware::getCoreId();
        setPStateFast(pStatus[core].base, core);
    }

    void setPerformanceFast() {
        int core = Hardware::getCoreId();
        setPStateFast(pStatus[core].fast, core);
    }

    void enableBoosting() {
        if (topo->isAMD()) {
            if (SysFSAmd15h::getPStatesBoostSrc() != 1)
                SysFSAmd15h::setPStatesBoostSrc(1);

            if (pAuto && !SysFSAmd15h::getPStatesMasterEnable())
                SysFSAmd15h::setPStatesMasterEnable(1);
            else if (pAuto == 0 && SysFSAmd15h::getPStatesMasterEnable())
                SysFSAmd15h::setPStatesMasterEnable(0);
        }

        for (auto p : topo->getPackages())
            msr->setBoostingPackage(p);
            
        for (auto c : topo->getCoresOnline())
            msr->setBoostingCore(c, true);

        boostingEnabled = true;
    }

    void disableBoosting() {
        for (auto c : topo->getCoresOnline())
            msr->setBoostingCore(c, false);

        boostingEnabled = false;
    }

    void manualBoosting(int pAuto = 0) {
        if (topo->isIntel())
            Debug::userError("PStates:manualBoosting:not supported on Intel");
        else if (topo->isAMD() && SysFSAmd15h::getPStatesLock())
            Debug::userError("PStates:manualBoosting:not supported on this AMD");

        if (boostingEnabled && this->pAuto == pAuto)
            return;

        if (pAuto < pStatus[0].fast || pAuto > pStatus[0].slow) {
            std::stringstream msg;
            msg << "PStates:manualBoosting:outOfRange " << pStatus[0].fast << "-" << pStatus[0].slow << ", requested " << pAuto;
            Debug::userError(msg.str().c_str());
        }

        SysFSAmd15h::setNumBoostedStates(pAuto);
        this->pAuto = pAuto;
        enableBoosting(); // update config

        if (SysFSAmd15h::getPStatesBoosted() != pAuto || msr->getPStateSlow(0) != SysFSAmd15h::getPStatesHwMaxVal() - pAuto) {
            std::stringstream msg;
            msg << "PStates:manualBoosting:Boosted " << SysFSAmd15h::getPStatesBoosted() << " SWmax " << msr->getPStateSlow(0) << " HWmax " << SysFSAmd15h::getPStatesHwMaxVal();
            Debug::userError(msg.str().c_str());
        }

        if (msr->getNbPstateDis(0) == 0)
            SysFSAmd15h::setSwNbPstateLoDis(1);
        
        if (SysFSAmd15h::getVSRampSlamTime() != 7)
            SysFSAmd15h::setVSRampSlamTime(7);

        for (auto c : topo->getCoresOnline())
            readPState(c);
    }

    void setVoltage(int pState, float mVolt, int core = 0) { msr->setVid(core, pState, (1550 - mVolt) * 10 / 125); }
};

}

#endif /* JOQUER_P_STATES_H_ */
