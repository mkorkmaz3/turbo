/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#ifndef JOQUER_H_
#define JOQUER_H_

#include <joquer/barrier.hpp>
#include <joquer/mutex.hpp>
#include <joquer/p_states.hpp>
#include <joquer/performance.hpp>
#include <joquer/thread_pool.hpp>

using namespace joquer;

class JoQueR {
public:
private:
    std::unique_ptr<ThreadPool> threadPool;
    std::shared_ptr<Topology> topo;
    std::shared_ptr<MSR> msr;
    std::unique_ptr<PStates> pstates;
    std::unique_ptr<Performance> perf;
    ClhLock fifoLock;

    JoQueR(bool enableHW) {
        topo = std::make_shared<Topology>();
        threadPool = std::unique_ptr<ThreadPool>(new ThreadPool(topo));
        if (enableHW) {
            msr = std::make_shared<MSR>(topo);
            pstates = std::unique_ptr<PStates>(new PStates(topo, msr));
            perf = std::unique_ptr<Performance>(new Performance(msr));
        }
    }

    JoQueR(JoQueR const&);
    void operator=(JoQueR const&);
public:
    static JoQueR& getInstance(bool enableHW = false) {
        static JoQueR instance(enableHW);
        return instance;
    }

    void createThreads(int numThreads = 0, Thread::workFunc startFunc = nullptr, void* startArg = 0) {
        if (startFunc == nullptr)
            threadPool->create(numThreads);
        else
            threadPool->create(numThreads, startFunc, startArg);
    }

    void recordThread(Thread::workFunc startFunc = nullptr, void* startArg = 0) { threadPool->record(startFunc, startArg); }

    int getThreadId() { return threadPool->getThread()->getThreadId(); }

    unsigned int getThreadCount() { return threadPool->getThreadCount(); }

    void setCpuAffinity() { threadPool->migrate(); }

    void sleepThread(unsigned int duration = 1000000) { threadPool->getThread()->sleep(duration); }

    void printCpuInfo(int times = 1, std::string cpus = "all") { perf->printCpuInfos(times, cpus); }

    void printPerformance() { perf->printPerformance(); }

    void performanceIntervalInit() {
        assert(Debug::userCheck("JoQueR:performanceIntervalInit no performance object initialized", pstates != nullptr));
        perf->initInterval();
    }

    void performanceIntervalSplit() { perf->splitInterval(); }

    void setBoosting(int boosted) {
        assert(Debug::userCheck("JoQueR:setBoosting no p-states object initialized", pstates != nullptr));
        if (topo->isAMD())
            pstates->manualBoosting(boosted);
    }

    void setPStates(int pstate, std::string cpus) { pstates->setPState(pstate, cpus); }

    void setPerformance(int pct) { pstates->setPerformance(pct); }

    Topology* getTopology() { return topo.get(); }
};

#endif /* JOQUER_H_ */
