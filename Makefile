SUBDIRS = test

.PHONY: $(SUBDIRS)

all: $(SUBDIRS)
     
$(SUBDIRS):
	$(MAKE) -C $@

clean:
	for i in $(SUBDIRS); do make -C $$i clean; done