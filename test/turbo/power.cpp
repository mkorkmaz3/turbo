/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <joquer.hpp>

int main(int argc, char *argv[]) {
  std::cout << "maximum processor TDP  : " << SysFSAmd15h::getMaxProcessorTDP() << " (" << SysFSAmd15h::getTDP2Watt(SysFSAmd15h::getMaxProcessorTDP()) << "mW)" << std::endl;
  std::cout << "maximum non-core TDP   : " << SysFSAmd15h::getMaxNonCoreTDP() << " (" << SysFSAmd15h::getTDP2Watt(SysFSAmd15h::getMaxNonCoreTDP()) << "mW)" << std::endl;
  std::cout << "maximum total cores TDP: " << SysFSAmd15h::getMaxTotalCoresTDP() << " (" << SysFSAmd15h::getTDP2Watt(SysFSAmd15h::getMaxTotalCoresTDP()) << "mW)" << std::endl;
  std::cout << "current processor TDP  : " << SysFSAmd15h::getCurrentProcessorTDP() << " (" << SysFSAmd15h::getTDP2Watt(SysFSAmd15h::getCurrentProcessorTDP()) << "mW)" << std::endl;

  return 0;
}
