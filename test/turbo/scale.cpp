/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <atomic>
#include <joquer.hpp>

class MCSLock {
    struct MCSNode {
        int32_t locked;
        std::atomic<MCSNode*> next;
        uint64_t tried;
        MCSNode* pred;
        uint64_t pad[5];
    };
    struct DelegateNode {
        std::atomic<int> pState;
        uint32_t pad[15];
    };
public:
    ThreadStorage<MCSNode> mcsNode;
    DelegateNode __attribute__ ((aligned (64))) delegate[4];
    std::atomic<MCSNode*> __attribute__ ((aligned (64))) tail;
    Futex futex;

    void init(int coreId) {
        MCSNode* node;
        
        int rc = posix_memalign((void**)&node, 64, sizeof(MCSNode));
        Debug::checkErrorCode("MCSLock:init:memalign", rc);

        node->locked = 0;
        node->next.store(NULL);
        node->tried = 0;
        node->pred = NULL;
        mcsNode.set(node);

        tail.store(NULL);
    }

    bool trylock() {
        MCSNode* node = mcsNode.get();

        node->next.store(NULL, std::memory_order_relaxed);
        node->pred = tail.exchange(node, std::memory_order_acq_rel);
        node->tried = 1;

        return node->pred == NULL;
    }

    void lock(bool useMwait = false, bool useFutex = false) {
        MCSNode* node = mcsNode.get();
        MCSNode* pred;

        if (node->tried == 1) {
            pred = node->pred;
            node->tried = 0;
        } else {
            node->next.store(NULL, std::memory_order_relaxed);
            pred = tail.exchange(node, std::memory_order_acq_rel);
        }

        if (pred != NULL) {
            node->locked = 1;
            pred->next.store(node, std::memory_order_release);
            while (node->locked == 1) {
                Hardware::compilerBarrier();
                if (useFutex) {
                    futex.lock(&node->locked, 1);
                } else if (useMwait) {
                    Hardware::monitor(&node->locked);
                    if (node->locked == 1)
                        Hardware::mwait();
                }   
            }
        }
    }

    void unlock(bool useFutex = false) {
        MCSNode* node = mcsNode.get();
        MCSNode* next = NULL;

        if (node->next.load(std::memory_order_acquire) == NULL) {
            MCSNode* n = node;

            if (tail.compare_exchange_strong(n, NULL, std::memory_order_acq_rel, std::memory_order_relaxed))
                return;

            while (node->next.load(std::memory_order_acquire) == NULL) {}
        }

        next = node->next.load(std::memory_order_acquire);
        next->locked = 0;

        if (useFutex)
            futex.unlock(&next->locked, 1);
    }

    void setDelegate(int packId, int pstate) { delegate[packId].pState.store(pstate, std::memory_order_release); }

    int getDelegate(int packId) { return delegate[packId].pState.load(std::memory_order_acquire); }
};

class Workload {
public:
    enum Types {
        STATIC_ONE_THREAD = 0,
        STATIC_ALL_THREAD,
        DYNAMIC_BOOST_OWNER,
        DYNAMIC_BOOST_ALL,
        STATIC_MIGRATE,
        STATIC_MWAIT,
        DELEGATE_BOOST_OWNER,
        DELEGATE_BOOST_ALL,
        STATIC_ALL_NO_SYNC,
        STATIC_ALL_MUTEX,
        STATIC_ALL_SPINLOCK,
        STATIC_ALL_FUTEX,
        NUM_TYPES
    };
    const char* token[NUM_TYPES] = {"STATIC_ONE_THREAD    ",
                                    "STATIC_ALL_THREAD    ",
                                    "DYNAMIC_BOOST_OWNER  ",
                                    "DYNAMIC_BOOST_ALL    ",
                                    "STATIC_MIGRATE       ",
                                    "STATIC_MWAIT         ",
                                    "DELEGATE_BOOST_OWNER ",
                                    "DELEGATE_BOOST_ALL   ",
                                    "STATIC_ALL_NO_SYNC   ",
                                    "STATIC_ALL_MUTEX     ",
                                    "STATIC_ALL_SPINLOCK  ",
                                    "STATIC_ALL_FUTEX     "};
private:
    MCSLock& mcs;
    Mutex& mutex;
    SpinLock& spin;
    MSR& msr;
    Topology& topo;
    Barrier& barrier;
    Thread* thread;
    int coreId;
    int packId;
    uint64_t freq;
    int pSlowest;
    int pDel;
    Types type;
    uint64_t duration; // k cycles
    uint64_t runtime; //s
    uint64_t iterations;
    uint64_t tsc;
    uint64_t mperf;
    uint64_t aperf;

    void work() {
        assert(type >= 0 && type < NUM_TYPES && duration != 0 && runtime != 0);
        volatile uint64_t tscCurr = 0;
        volatile uint64_t tscWait = (freq * runtime * 1000000);
        volatile uint64_t tscStart = Hardware::readTSC();
        tscWait += tscStart;
        do {
            switch (type) {
                case STATIC_ONE_THREAD:
                    if (coreId == 0) {
                        mcs.lock();
                        iteration();
                        mcs.unlock();
                        iterations++;
                    } else {
                        iteration();
                    }
                    break;
                case STATIC_ALL_THREAD:
                    mcs.lock();
                    iteration();
                    mcs.unlock();
                    iterations++;
                    break;
                case DYNAMIC_BOOST_OWNER:
                    mcs.lock();
                    pFast();
                    iteration();
                    mcs.unlock();
                    pSlow();
                    iterations++;
                    break;
                case DYNAMIC_BOOST_ALL:
                    if (!mcs.trylock()) {
                        pSlow();
                        mcs.lock();
                        pFast();
                    }
                    iteration();
                    mcs.unlock();
                    iterations++;
                    break;
                case STATIC_MIGRATE:
                    if (coreId > 1) {
                        mcs.lock();
                        thread->migrate(0);
                        iteration();
                        thread->migrate(coreId);
                        mcs.unlock();
                        iterations++;
                    } else if (coreId == 1) {
                        iteration();
                    } else {
                        return;
                    }
                    break;
                case STATIC_MWAIT:
                    mcs.lock(true);
                    iteration();
                    mcs.unlock();
                    iterations++;
                    break;
                case DELEGATE_BOOST_OWNER:
                    if (coreId % 2 == 0) {
                        mcs.lock();
                        dFast();
                        iteration();
                        mcs.unlock();
                        dSlow();
                        iterations++;
                    } else {
                        delegate();
                    }
                    break;
                case DELEGATE_BOOST_ALL:
                    if (coreId % 2 == 0) {
                        if (!mcs.trylock()) {
                            dSlow();
                            mcs.lock();
                            dFast();
                        }
                        iteration();
                        mcs.unlock();
                        iterations++;
                    } else {
                        delegate();
                    }
                    break;
                case STATIC_ALL_NO_SYNC:
                    iteration();
                    iterations++;
                    break;
                case STATIC_ALL_MUTEX:
                    mutex.lock();
                    iteration();
                    mutex.unlock();
                    iterations++;
                    break;
                case STATIC_ALL_SPINLOCK:
                    spin.lock();
                    iteration();
                    spin.unlock();
                    iterations++;
                    break;
                case STATIC_ALL_FUTEX:
                    mcs.lock(false, true);
                    iteration();
                    mcs.unlock(true);
                    iterations++;
                    break;
                default:
                    Debug::userError("Workload:work:unknown type");
            }
            tscCurr = Hardware::readTSC();
        } while (tscCurr < tscWait);
        mcs.setDelegate(packId, -1);
        tsc = tscCurr - tscStart;
    }

    void iteration() __attribute__((noinline,optimize(0))) {
        for (uint64_t i = 0; i < duration; i++) {}
        asm ("");
    }

    void delegate() {
        volatile int pCurr = pDel;
        while (true) {
            pCurr = mcs.getDelegate(packId);
            if (pCurr == -1)
                return;
            if (pCurr != pDel) {
                msr.setPState(pCurr);
                pDel = pCurr;
            }
        }
    }

    void pFast() { msr.setPState(0); }

    void pSlow() { msr.setPState(pSlowest); }

    void dFast() { mcs.setDelegate(packId, 0); }

    void dSlow() { mcs.setDelegate(packId, pSlowest); }

    void recordTime() { 
        mperf = msr.getMPerf() - mperf;
        aperf = msr.getAPerf() - aperf;
    }
public:
    Workload(MCSLock& l, Mutex& mu, SpinLock& sl, MSR& m, Topology& t, Barrier& b) : mcs(l), mutex(mu), spin(sl), msr(m), topo(t), barrier(b), thread(NULL), coreId(-1), packId(-1), freq(0), pSlowest(0), type(NUM_TYPES), duration(0), runtime(0), iterations(0), tsc(0), mperf(0), aperf(0) {}

    void config(Types t, uint64_t d, uint64_t r) {
        type = t;
        duration = d * 10; // 1 iteration takes about 10 cycles
        runtime = r;
    }

    uint64_t getFreq() { return freq; } //P0

    uint64_t getIterations() { return iterations; }

    uint64_t getTsc() { return tsc; }

    uint64_t getMperf() { return mperf; }

    uint64_t getAperf() { return aperf; }

    uint64_t getIntervalFreq() { return (aperf * freq) / mperf; } //MHz

    uint64_t getIntervalTimeC0() { return (mperf > tsc) ? (tsc / freq) : (mperf / freq); } //us 

    uint64_t getIntervalTimeCx() { return (mperf > tsc) ? 0 : (tsc - mperf) / freq; } //us

    static void workEnter(void* arg, Thread* th) {
        Workload* w = static_cast<Workload*>(arg);
        w->thread = th;
        th->migrate(th->getThreadId());
        w->coreId = th->getCoreId();
        w->packId = w->coreId /2;
        if (w->topo.isIntel())
            w->freq = w->msr.getPStateBase(w->coreId) * 100;
        else {
            int pBoost = SysFSAmd15h::getPStatesBoosted();
            w->freq = (w->msr.getFid(w->coreId, pBoost) + 16) * 200 / (2 << w->msr.getDid(w->coreId, pBoost)); //MHz
        }
        w->pSlowest = w->msr.getPStateSlow(w->coreId);
        w->pDel = w->pSlowest;
        w->mcs.setDelegate(w->packId, 0);
        if (!w->msr.getMonMwaitUserEnable(w->coreId))
            w->msr.setMonMwaitUserEnable(w->coreId);
        if ((w->type == DELEGATE_BOOST_OWNER || w->type == DELEGATE_BOOST_ALL) && w->coreId % 2 == 0)
            w->pSlow();
        w->mcs.init(w->coreId);
        w->barrier.wait();
        w->recordTime();
        w->work();
        w->recordTime();
        w->barrier.wait();
    }

    void printType() {
        std::cout << "Workload " << token[type] << std::endl;
    }
};

static const int NB_CORE = 8;
static const int NB_SAMPLE = 20;

int main(int argc, char * argv[]) {
    MCSLock mcs;
    Mutex mutex;
    SpinLock spin;
    MSR msr;
    Topology topo;
    Barrier barrier(NB_CORE + 1);
    Thread mainTh(0);
    Thread workerTh[NB_CORE] = {{0},{1},{2},{3},{4},{5},{6},{7}};
    Workload wl[NB_CORE] = {{mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}, {mcs, mutex, spin, msr, topo, barrier}};

    Workload::Types wlType = Workload::Types::STATIC_ONE_THREAD;
    uint64_t wlDuration = 10; //iterations
    uint64_t totalRuntime = 10; //s
    int samplePower = 0;
    unsigned int powerSamples[NB_SAMPLE];

    if (argc < 4) {
        Debug::userWarning("Arguments: workloadType workloadDuration(iterations) totalRuntime(s)");
    } else {
        wlType = static_cast<Workload::Types>(Helper::stringToNumber<unsigned int>(argv[1]));
        wlDuration = Helper::stringToNumber<uint64_t>(argv[2]);
        totalRuntime = Helper::stringToNumber<uint64_t>(argv[3]);
        if (argc > 4)
            samplePower = Helper::stringToNumber<int>(argv[4]);
    }

    mainTh.record();
    mainTh.migrate(NB_CORE - 1);
    
    for (int t = 0; t < NB_CORE; t++) {
        wl[t].config(wlType, wlDuration, totalRuntime);
        workerTh[t].create(Workload::workEnter, &wl[t]);
    }

    barrier.wait();
    uint64_t actualRuntime = Hardware::readTSC();
    if (samplePower && topo.isAMD()) {
        for (int i = 0; i < NB_SAMPLE; i++) {
            mainTh.sleep(samplePower * 1000);
            powerSamples[i] = SysFSAmd15h::getTDP2Watt(SysFSAmd15h::getCurrentProcessorTDP());
        }
    }
    barrier.wait();
    actualRuntime = Hardware::readTSC() - actualRuntime;

    uint64_t iterations = 0;
    uint64_t cycles = 0;
    uint64_t aperf = 0;
    uint64_t mperf = 0;

    wl[0].printType();
    std::cout << "Th Iterations       Freq         C0         Cx" << std::endl;
    for (int t = 0; t < NB_CORE; t++) {
        workerTh[t].join();
        iterations += wl[t].getIterations();
        cycles += wl[t].getTsc();
        aperf += wl[t].getAperf();
        mperf += wl[t].getMperf();
        std::cout << std::setw(2) << t << std::setw(11) << wl[t].getIterations() << std::setw(11) << wl[t].getIntervalFreq() << std::setw(11) << wl[t].getIntervalTimeC0() << std::setw(11) << wl[t].getIntervalTimeCx() << std::endl;
    }

    std::cout << "Runtime (ms) " << actualRuntime / (wl[7].getFreq() * 1000) << std::endl;
    std::cout << "Iterations " << iterations << std::endl;
    std::cout << "Cycles " << cycles << std::endl;
    std::cout << "Aperf " << aperf << std::endl;
    std::cout << "Mperf " << mperf << std::endl;

    if (samplePower) {
        uint64_t powerTotal = 0;
        for (int i = 0; i < NB_SAMPLE; i++)
            powerTotal += powerSamples[i];
        std::cout << "Power " << powerTotal / NB_SAMPLE << std::endl;
    } 

    return EXIT_SUCCESS;
}
