/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include "config.hpp"

namespace turbo {

class Turbo {
private:
    static Config* config;
public:
    Turbo(int argc, char **argv) {
        JoQueR::getInstance(true);
        config = new Config(argc, argv);
    }

    void run() {
        JoQueR& runtime = JoQueR::getInstance(true);
        switch (config->executionMode) {
            case Mode::MODE_INFO:
                runtime.printCpuInfo(config->iterations < 1 ? 1 : config->iterations, config->selectCpus);
                break;
            case Mode::MODE_CONFIG:
                runtime.setBoosting(config->bState);
                runtime.setPStates(config->pState, config->selectCpus);
                break;
            default:
                return;
        }
    }
};

Config* Turbo::config;
}

int main(int argc, char * argv[]) {
    turbo::Turbo t(argc, argv);
    t.run();

    return 0;
}

