/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <atomic>

#include <joquer.hpp>
#include "joquer.h"

struct profile_data {
    uint64_t tscTotal;
    uint64_t tscSquare;
    uint64_t tscMin;
    uint64_t tscMax;
    uint64_t count;
};

struct thread_data {
    trb_thread_type_t type;
    int coreId;
    bool started;
    bool waited;
    uint64_t tscTmp;
    profile_data prof[TRB_EVENT_MAX];
};

typedef JoQueR::PerformanceType PerformanceType;

static const bool trb_profile = true;
static const int max_threads = 8;
static int max_worker = max_threads;
static int trb_pstate = 0;

static MSR* msr;
static std::atomic<int> workers(0);
static int exited = 0;
static __thread thread_data trb_info;
static profile_data trb_infos[TRB_EVENT_MAX];

void trb_update_profile(profile_data& p, uint64_t cycles) {
    p.tscTotal += cycles;
    p.tscSquare += (cycles * cycles);
    if (p.tscMin == 0 || cycles < p.tscMin)
        p.tscMin = cycles;
    if (cycles > p.tscMax)
        p.tscMax = cycles;
    p.count++;
}

void trb_print_profile(profile_data* p) {
    printf("type   count        total         mean      dev          min          max\n");
    for (int e = 0; e < TRB_EVENT_MAX; e++) {
        uint64_t mean   = p[e].count > 0 ? p[e].tscTotal / p[e].count : 0;
        uint64_t dev    = p[e].count > 1 ? sqrt(abs(trb_infos[e].tscSquare - (p[e].tscTotal * p[e].tscTotal) / p[e].count) / (p[e].count - 1)) : 0;
        if (e == TRB_ACQUIRE)
            printf("WAIT");
        if (e == TRB_OWN)
            printf(" OWN");
        if (e == TRB_RELEASE)
            printf("NATV");
        printf(" %7lu %12lu %12lu %8lu %12lu %12lu\n", 
                p[e].count, p[e].tscTotal, mean, dev, p[e].tscMin, p[e].tscMax);
    }
}

void trb_set_affinity(int coreId) {
    cpu_set_t set;
    CPU_ZERO(&set);
    CPU_SET(coreId, &set);
    int rc = pthread_setaffinity_np(pthread_self(), sizeof(set), &set); 
    Debug::checkReturnCode("lib:pthread_setaffinity_np", rc);
    trb_info.coreId = coreId;
}

void trb_exit(void) {
    if (trb_pstate == 1) {
        msr->setPState(2);
    }
    if (trb_pstate)
        delete(msr);
}

void trb_exit_thread(trb_thread_type_t source) {
    exited++;
    if (trb_profile) {
        /*if (exited == workers.load())
            printf("  count        total         mean      dev          min          max\n");*/
        for (int e = 0; e < TRB_EVENT_MAX; e++) {
            trb_infos[e].tscTotal += trb_info.prof[e].tscTotal;
            trb_infos[e].tscSquare += trb_info.prof[e].tscSquare;
            if (trb_infos[e].tscMin == 0 || trb_info.prof[e].tscMin < trb_infos[e].tscMin)
                trb_infos[e].tscMin = trb_info.prof[e].tscMin;
            if (trb_info.prof[e].tscMax > trb_infos[e].tscMax)
                trb_infos[e].tscMax = trb_info.prof[e].tscMax;
            trb_infos[e].count += trb_info.prof[e].count;

            /*if (exited == workers.load()) {
                uint64_t mean   = trb_infos[e].count > 0 ? trb_infos[e].tscTotal / trb_infos[e].count : 0;
                uint64_t dev    = trb_infos[e].count > 1 ? sqrt(abs(trb_infos[e].tscSquare - (trb_infos[e].tscTotal * trb_infos[e].tscTotal) / trb_infos[e].count) / (trb_infos[e].count - 1)) : 0;
                printf("%7lu %12lu %12lu %8lu %12lu %12lu\n", 
                        trb_infos[e].count, trb_infos[e].tscTotal, mean, dev, trb_infos[e].tscMin, trb_infos[e].tscMax);
            }*/
        }
    }
    if (exited == workers.load()) {
        if (trb_profile)
            trb_print_profile(trb_infos);
        exited = 0;
        workers.store(0);
    }
    if (trb_pstate)
        msr->setPState(6);
}

void trb_init_thread(trb_thread_type_t type) {
    if (type == TRB_MAIN) {
        char *env = getenv("TRB_PSTATE_ENABLE");
        if (env != NULL)
            trb_pstate = atoi(env);
        if (trb_pstate)
            msr = new MSR();

        trb_set_affinity(max_threads - 1);
        
        atexit(trb_exit);
    }
    
    if (type == TRB_WORKER) {
        int wId = workers.fetch_add(1);
        int coreId = ((wId * 2) % max_worker) + ((wId * 2) / max_worker);
        trb_set_affinity(coreId);
        
        if (trb_pstate == 1 || trb_pstate == 3)
            msr->setPState(0);
        else if (trb_pstate == 2)
            msr->setPState(2);
    }
    
    trb_info.type = type;
    trb_info.tscTmp = 0;
    trb_info.started = false;
    trb_info.waited = false;
    for (int i = 0; i < TRB_EVENT_MAX && trb_profile; i++) {
        trb_info.prof[i].tscTotal = 0;
        trb_info.prof[i].tscSquare = 0;
        trb_info.prof[i].tscMin = 0;
        trb_info.prof[i].tscMax = 0;
        trb_info.prof[i].count = 0;
    }
}

void trb_event(trb_event_type_t event) {
    uint64_t tscCurr;
    switch (event) {
        case TRB_START:
            trb_info.started = true;
            if (trb_profile)
                trb_info.tscTmp = Hardware::readTSC();
            break;
        case TRB_ACQUIRE:
            if (trb_profile && trb_info.started) {
                tscCurr = Hardware::readTSC();
                trb_update_profile(trb_info.prof[TRB_RELEASE], tscCurr - trb_info.tscTmp);
                trb_info.tscTmp = tscCurr;
            }
            break;
        case TRB_WAIT:
            if (trb_info.type == TRB_WORKER && (trb_pstate == 1 || trb_pstate == 3))
                msr->setPState(6);
            if (trb_profile || trb_pstate == 3)
                trb_info.waited = true;
            break;
        case TRB_OWN:
            if (trb_info.type == TRB_WORKER) {
                if ((trb_pstate == 3 && trb_info.waited) || trb_pstate == 1)
                    msr->setPState(0);
                if (trb_pstate == 2)
                    msr->setPState(1);
            }
            if (trb_profile && trb_info.started && trb_info.waited) {
                tscCurr = Hardware::readTSC();
                trb_update_profile(trb_info.prof[TRB_ACQUIRE], tscCurr - trb_info.tscTmp);
                trb_info.tscTmp = tscCurr;
            }
            if (trb_profile || trb_pstate == 3)
                trb_info.waited = false;
            break;
        case TRB_RELEASE:
            if (trb_info.type == TRB_WORKER && (trb_pstate == 1 || trb_pstate == 2))
                msr->setPState(2);
            if (trb_profile && trb_info.started) {
                tscCurr = Hardware::readTSC();
                trb_update_profile(trb_info.prof[TRB_OWN], tscCurr - trb_info.tscTmp);
                trb_info.tscTmp = tscCurr;
            }
            break;
        case TRB_STOP:
            /*if (trb_info.type == TRB_MAIN) {
                if (trb_profile)
                    printf("cycles %lu\n", Hardware::readTSC() - trb_info.tscTmp);
                    //trb_print_profile(trb_info.prof);
            }*/
            trb_info.started = false;
            break;
        case TRB_MARK_CLIENT:
            trb_set_affinity(max_threads - 2);
            if (trb_pstate)
                msr->setPState(4);
            
            trb_info.type = TRB_CLIENT;
            break;
        case TRB_EXIST_CLIENT:
            max_worker = 6;
            break;
        default:
            Debug::userError("trb_event unkown event");
            break;
    }
}

