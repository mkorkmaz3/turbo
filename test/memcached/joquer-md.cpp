/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <atomic>

#include <joquer.hpp>
#include <joquer.h>

typedef struct profile_data {
    uint64_t tscTotal;
    uint64_t tscSquare;
    uint64_t tscMin;
    uint64_t tscMax;
    uint64_t tscTmp;
    uint64_t count;
    uint64_t stalledFront;
    uint64_t stalledBack;
    uint64_t cycles;
    uint64_t mperf;
    uint64_t aperf;
    std::atomic<uint64_t> finds;
    std::atomic<uint64_t> inserts;
    std::atomic<uint64_t> deletes;
} profile_data_t;

typedef struct info_data {
    trb_thread_type_t type;
    int pstate;
    bool profiling;
    bool resizing;
    uint64_t finds;
    uint64_t inserts;
    uint64_t deletes;
} info_data_t;

typedef JoQueR::PerformanceType PerformanceType;

static int resizeCount = -1;
static const int resizeCountMax = 10;
static profile_data_t pd[resizeCountMax];
static MSR* msr;
static PerformanceType* perf;
static PerfEvent* peFront;
static PerfEvent* peBack;

static std::atomic_flag initialized = ATOMIC_FLAG_INIT;
static std::atomic<int> workers(0);
static std::atomic<int> resizing(0);
static __thread info_data_t trb_info;
static uint64_t tscWorkload = 0;

static const bool trb_profile = false;
static const bool trb_eval = true;
static int trb_pstate = 0;


void trb_exit(void) {
    if (trb_profile || trb_eval) {
        FILE* outfile = fopen("stats.txt", "w");

        fprintf(outfile, "cycles workload %lu\n", tscWorkload);
        fprintf(outfile, "pow   count        total         mean      dev          min          max stallFront  stallBack       cycles       gets       sets       dels size freq\n");
        for (int i = 0; i < resizeCount; i++) {
            uint64_t mean   = pd[i].count > 0 ? pd[i].tscTotal / pd[i].count : 0;
            uint64_t dev    = pd[i].count > 1 ? sqrt(abs(pd[i].tscSquare - (pd[i].tscTotal * pd[i].tscTotal) / pd[i].count) / (pd[i].count - 1)) : 0;
            uint64_t fracF  = pd[i].cycles > 0 ? (pd[i].stalledFront * 100) / pd[i].cycles : 0;
            uint64_t fracB  = pd[i].cycles > 0 ? (pd[i].stalledBack * 100) / pd[i].cycles : 0;
            uint64_t size   = (((((2 << (16 + i)) * 3) / 2) * 48) + ((2 << (16 + i)) * sizeof(void*))) / 1024 / 1024;
            uint64_t freq   = (pd[i].aperf * 4000) / pd[i].mperf;
            fprintf(outfile, "%3d %7lu %12lu %12lu %8lu %12lu %12lu %9lu%% %9lu%% %12lu %10lu %10lu %10lu %4lu %4lu\n", 
                16 + i, pd[i].count, pd[i].tscTotal, mean, dev, pd[i].tscMin, pd[i].tscMax, fracF, fracB, pd[i].cycles, pd[i].finds.load(), pd[i].inserts.load(), pd[i].deletes.load(), size, freq);
        }

        fclose(outfile);

        delete(msr);

        if (trb_eval) {
            delete(peFront);
            delete(peBack);
            delete(perf);
        }
    }
}

void trb_init_thread(trb_thread_type_t type) {
    if (!initialized.test_and_set()) {
        char *env = getenv("TRB_PSTATE_ENABLE");
        if (env != nullptr)
            trb_pstate = atoi(env);
        atexit(trb_exit);
        msr = new MSR();
        if (trb_eval)
            perf = new PerformanceType(nullptr, msr);
    }

    int coreId;
    if (type == TRB_MAIN) {
        coreId = 1;
    } else if (type == TRB_WORKER) {
        coreId = (workers.fetch_add(1) % 4) + 2;
    } else if (type == TRB_MAINTENANCE) {
        coreId = 7;
        if (trb_eval) {
            peFront = new PerfEvent(7, getpid(), PERF_COUNT_HW_CPU_CYCLES);
            peBack = new PerfEvent(7, getpid(), PERF_COUNT_HW_STALLED_CYCLES_BACKEND);
        }
    } else if (type == TRB_CLIENT) {
        coreId = 0;
    } else
        Debug::userError("lib:trb_init_thread: thread type not supported");

    cpu_set_t set;
    CPU_ZERO(&set);
    CPU_SET(coreId, &set);
    int rc = pthread_setaffinity_np(pthread_self(), sizeof(set), &set); 
    Debug::checkReturnCode("lib:pthread_setaffinity_np", rc);
    
    trb_info.type = type;
    trb_info.pstate = 2;
    trb_info.profiling = false;
    trb_info.resizing = false;
    trb_info.finds = 0;
    trb_info.inserts = 0;
    trb_info.deletes = 0;
}

void trb_mutex_lock(pthread_mutex_t* mutex, trb_thread_type_t source) {
    if (pthread_mutex_trylock(mutex) != 0) {
        if (trb_pstate == 2 && trb_info.type != TRB_MAINTENANCE && resizing.load(std::memory_order_acquire) && trb_info.pstate != 6) {
            msr->setPState(6);
            trb_info.pstate = 6;
        }

        if (source == TRB_WORKER_NT)
            pthread_mutex_lock(mutex);
        else
            while (pthread_mutex_trylock(mutex) != 0);
    }

    if (trb_info.type == TRB_MAINTENANCE && trb_info.resizing) {
        if (trb_pstate && trb_info.pstate != 0) {
            msr->setPState(0);
            trb_info.pstate = 0;
        }

        if (trb_profile && !trb_info.profiling) {
            pd[resizeCount].tscTmp = Hardware::readTSC();
            trb_info.profiling = true;
        }
    } else if (trb_info.pstate != 2) {
        msr->setPState(2);
        trb_info.pstate = 2;
    }
}

void trb_mutex_unlock(pthread_mutex_t* mutex, trb_thread_type_t source) {
    if (trb_info.profiling) {
        uint64_t now = Hardware::readTSC();
        if (now - pd[resizeCount].tscTmp > 87559781686169)
            printf("old %lu new %lu diff %lu\n", pd[resizeCount].tscTmp, now, now - pd[resizeCount].tscTmp);
        uint64_t curr = now - pd[resizeCount].tscTmp; 
        
        pd[resizeCount].count++;
        if (curr > pd[resizeCount].tscMax) pd[resizeCount].tscMax = curr;
        if (pd[resizeCount].tscMin == 0) pd[resizeCount].tscMin = curr;
        else if (curr < pd[resizeCount].tscMin) pd[resizeCount].tscMin = curr;
        pd[resizeCount].tscTotal += curr;
        pd[resizeCount].tscSquare += (curr * curr);
        trb_info.profiling = false;
    }
    if (trb_info.pstate != 2) {
        msr->setPState(2);
        trb_info.pstate = 2;
    }
    pthread_mutex_unlock(mutex);
}

void trb_mutex_event(trb_event_type_t event, trb_thread_type_t source) {
    switch (event) {
        case  TRB_SKIP:
            if (trb_profile)
                trb_info.profiling = false;
            break;
        case TRB_RESIZE_START:
            trb_info.resizing = true;
            if (trb_info.type == TRB_MAINTENANCE) {
                resizing.store(1, std::memory_order_release);
                if (trb_eval) {
                    pd[resizeCount].cycles = Hardware::readTSC();
                    pd[resizeCount].mperf = msr->getMPerf();
                    pd[resizeCount].aperf = msr->getAPerf();
                    if (resizeCount == 0)
                        tscWorkload = pd[resizeCount].cycles;
                    peFront->resetCounter();
                    peBack->resetCounter();
                }
            }
            break;
        case TRB_RESIZE_STOP:
            trb_info.resizing = false;
            if (trb_info.type == TRB_MAINTENANCE) {
                resizing.store(0, std::memory_order_release);
                if (trb_eval && resizeCount != -1) {
                    pd[resizeCount].cycles = Hardware::readTSC() - pd[resizeCount].cycles;
                    pd[resizeCount].mperf = msr->getMPerf() - pd[resizeCount].mperf;
                    pd[resizeCount].aperf = msr->getAPerf() - pd[resizeCount].aperf;
                    pd[resizeCount].stalledFront = peFront->getCounter();
                    pd[resizeCount].stalledBack = peBack->getCounter();
                }
                if (trb_profile || trb_eval) {
                    resizeCount++;
                    if (resizeCount >= resizeCountMax)
                        Debug::userError("trb_mutex_event:resize_stop:overflow");
                    if (resizeCount > 7)
                        tscWorkload = Hardware::readTSC() - tscWorkload;
                }
            } else {
                if (trb_eval && resizeCount > 0) {
                    pd[resizeCount - 1].finds += trb_info.finds;
                    pd[resizeCount - 1].inserts += trb_info.inserts;
                    pd[resizeCount - 1].deletes += trb_info.deletes;
                }
            }
            break;
        case TRB_RESIZE_EXIT:
            if (resizeCount > 7)
                exit(EXIT_SUCCESS);
            break;
        case TRB_FIND:
            if (trb_eval && resizing.load(std::memory_order_acquire))
                trb_info.finds++;
            break;
        case TRB_INSERT:
            if (trb_eval && resizing.load(std::memory_order_acquire))
                trb_info.inserts++;
            break;
        case TRB_DELETE:
            if (trb_eval && resizing.load(std::memory_order_acquire))
                trb_info.deletes++;
            break;
        default:
            break;
    }
}

