/* Copyright (c) 2014 Jons-Tobias Wamhoff

 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#include <pthread.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum trb_thread_type {
    TRB_MAIN = 0,
    TRB_WORKER,
    TRB_WORKER_NT,
    TRB_WORKER_ITEM,
    TRB_MAINTENANCE,
    TRB_MAINTENANCE_ITEM,
    TRB_CLIENT 
} trb_thread_type_t;

typedef enum trb_event_type {
    TRB_SKIP = 0,
    TRB_RESIZE_START,
    TRB_RESIZE_STOP,
    TRB_RESIZE_EXIT,
    TRB_RESIZE,
    TRB_FIND,
    TRB_INSERT,
    TRB_DELETE
} trb_event_type_t;

void trb_init_thread(trb_thread_type_t source);

void trb_mutex_lock(pthread_mutex_t* mutex, trb_thread_type_t soure);

void trb_mutex_unlock(pthread_mutex_t* mutex, trb_thread_type_t soure);

void trb_mutex_event(trb_event_type_t event, trb_thread_type_t source);

#ifdef __cplusplus
}
#endif
