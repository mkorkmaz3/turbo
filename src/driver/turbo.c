/*
 * Copyright (C) 2013 Jons-Tobias Wamhoff
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ioctl numbers:                https://www.kernel.org/doc/Documentation/ioctl/ioctl-number.txt
 */

#include <asm/uaccess.h>
#include <asm/msr.h>
#include <asm/msr-index.h>
#include <asm/mwait.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/module.h>

#include <joquer/driver.h>

#ifdef TRB_DEBUG
# define TRB_DBG(...) printk(KERN_DEBUG __VA_ARGS__)
#elif defined(TRB_INFO)
# define TRB_DBG(...) printk(KERN_INFO __VA_ARGS__)
#else
# define TRB_DBG(...)
#endif

#ifndef MSR_AMD_COFVID_STATUS
# define MSR_AMD_COFVID_STATUS 0xc0010071
#else
# warning "MSR_AMD_COFVID_STATUS already defined"
#endif

static unsigned int is_intel, pmin, pmax, pturbo;
static volatile u64 mwait_value;

static int trb_open(struct inode* inode, struct file* file) {
    TRB_DBG("Turbo open\n");
    return 0;
}

static int trb_release(struct inode* inode, struct file* file) {
    TRB_DBG("Turbo close\n");
    return 0;
}

#ifdef RDTSC_GCC
# include <x86intrin.h>
# define trb_rdtscp() ({ unsigned int aux; _rdtscp(&aux); })
#elif defined(RDTSC_KERNEL)
/* 3.8 kernel is buggy... */
# define trb_rdtscp() ({ unsigned int aux; unsigned long long val; rdtscpll(val, aux); val;})
#else
static inline unsigned long long trb_rdtscp(void) {
    unsigned long long tsc;
    asm volatile ("rdtscp; "         // serializing read of tsc
                  "shl $32,%%rdx; "  // shift higher 32 bits stored in rdx up
                  "or %%rdx,%%rax"   // and or onto rax
                  : "=a"(tsc)        // output to tsc variable
                  :
                  : "%rcx", "%rdx"); // rcx and rdx are clobbered
    return tsc;
}
#endif

static unsigned long long trb_get_bits(unsigned long long word, int from, int till) {
    unsigned long long mask = ((1ULL << (till + 1 - from)) - 1) << from;
    return (word & mask) >> from;
}

static long trb_ioctl(struct file* file, unsigned int cmd, unsigned long arg) {
    int ret = 0;
    int i = 0;
    unsigned long long msr;
    unsigned long long fid_did_def;
    unsigned pstate;

    switch(cmd) {
        case TURBO_IOC_GET_MPERF:
            msr = native_read_msr(MSR_IA32_MPERF); // native_read_pmc
            TRB_DBG("Turbo ioctl get mperf %llu\n", msr);
            ret = __put_user(msr, (u64 __user *)arg);
            break;
        case TURBO_IOC_GET_APERF:
            msr = native_read_msr(MSR_IA32_APERF);
            TRB_DBG("Turbo ioctl get aperf %llu\n", msr);
            ret = __put_user(msr, (u64 __user *)arg);
            break;
        case TURBO_IOC_GET_PSTATE:
            if (is_intel) {
                msr = native_read_msr(MSR_IA32_PERF_STATUS);
                msr = (msr >> 8) & 0xFF;
            } else
                msr = native_read_msr(MSR_AMD_PERF_STATUS);
            TRB_DBG("Turbo ioctl get pstate %llu\n", msr);
            ret = (long)msr;
            break;
        case TURBO_IOC_SET_PSTATE:
            pstate = (unsigned __user)arg;
            if (is_intel) {
                pstate = clamp_t(unsigned, pstate, pmin, pturbo);
                // This clears the disengage IDA bit
                native_write_msr(MSR_IA32_PERF_CTL, pstate << 8, 0);
            } else
                native_write_msr(MSR_AMD_PERF_CTL, pstate, 0);
            TRB_DBG("Turbo ioctl set pstate %u\n", pstate);
            break;
        case TURBO_IOC_TSC_PSTATE:
            if (!access_ok(VERIFY_WRITE, (unsigned __user *)arg, _IOC_SIZE(cmd)))
                return -EFAULT;

            ret = __get_user(pstate, (unsigned __user *)arg);
            if (ret != 0)
                break;

            if (is_intel) {
                pstate = clamp_t(unsigned, pstate, pmin, pturbo);
                // This clears the disengage IDA bit
                msr = trb_rdtscp();
                native_write_msr(MSR_IA32_PERF_CTL, pstate << 8, 0);
                msr = trb_rdtscp() - msr;
            } else {
                msr = trb_rdtscp();
                native_write_msr(MSR_AMD_PERF_CTL, pstate, 0);
                msr = trb_rdtscp() - msr;
            }
            TRB_DBG("Turbo ioctl tsc pstate %u cycles %llu\n", pstate, msr);

            ret = __put_user(msr, (u64 __user *)arg);
            break;
        case TURBO_IOC_WAIT_PSTATE:
            if (!access_ok(VERIFY_WRITE, (unsigned __user *)arg, _IOC_SIZE(cmd)))
                return -EFAULT;

            ret = __get_user(pstate, (unsigned __user *)arg);
            if (ret != 0)
                break;

            if (is_intel)
                pstate = clamp_t(unsigned, pstate, pmin, pturbo);
            else
                fid_did_def = trb_get_bits(native_read_msr(MSR_AMD_PSTATE_DEF_BASE + pstate), 0, 8);

            msr = trb_rdtscp();

            if (is_intel) {
                unsigned pctl = pstate << 8;
                // This clears the disengage IDA bit
                native_write_msr(MSR_IA32_PERF_CTL, pctl, 0);
                while ((native_read_msr(MSR_IA32_PERF_STATUS) & 0xFFFF) != pctl && i <= TURBO_MAX_WAIT) { i++; }
            } else {
                native_write_msr(MSR_AMD_PERF_CTL, pstate, 0);
                while (trb_get_bits(native_read_msr(MSR_AMD_COFVID_STATUS), 0, 8) != fid_did_def && i <= TURBO_MAX_WAIT) { i++; }
            }

            msr = trb_rdtscp() - msr;
            TRB_DBG("Turbo ioctl wait pstate %u cycles %llu iterations %d\n", pstate, msr, i);

            ret = __put_user(msr, (u64 __user *)arg);
            if (ret == 0)
                ret = i;
            break;
        case TURBO_IOC_DUMMY:
            TRB_DBG("Turbo ioctl dummy\n");
            break;
        case TURBO_IOC_MWAIT:
            /* Read asked C-state */
            ret = __get_user(msr, (u64 __user *)arg);
            if (ret) break;
            __monitor((void *)&mwait_value, 0, 0);
            smp_mb();
            __mwait(msr, 0); /* cstate value: 0 -> C1, 2 -> C3, 5 -> C6, 15 -> C0 */
            msr = trb_rdtscp();
            /* Give the TSC when it woke it */
            ret = __put_user(msr, (u64 __user *)arg);
            break;
        case TURBO_IOC_MSPIN:
            msr = trb_rdtscp();
            for(i = 0; i < 1000000; i++)
                mwait_value++;
            /* Give the TSC when it tried to wake up the core */
            ret = __put_user(msr, (u64 __user *)arg);
            break;
        default:
            TRB_DBG("Turbo ioctl default\n");
            return -ENOTTY;
    }
    return ret;
}

static const struct file_operations trb_fops = {
    .owner          = THIS_MODULE,
    .open           = trb_open,
    .release        = trb_release,
    .unlocked_ioctl = trb_ioctl
};

static struct miscdevice trb_mdev = {
    .minor = MISC_DYNAMIC_MINOR,
    .name  = KBUILD_MODNAME,
    .fops  = &trb_fops,
};

static void trb_cleanup(void) {
    TRB_DBG("Turbo cleanup module\n");
    if (trb_mdev.this_device)
        misc_deregister(&trb_mdev);
}

static int trb_init(void) {
    int cpu, ret = 0;
    unsigned long long value;

    ret = misc_register(&trb_mdev);
    TRB_DBG("Turbo init module\n");
    if (ret < 0) {
        pr_err("Turbo misc_register failed\n");
        goto exit;
    }

    switch (boot_cpu_data.x86_vendor) {
        case X86_VENDOR_INTEL:
            TRB_DBG("Turbo vendor Intel\n");
            is_intel = 1;

            // FIXME Should check for NHM or HSW
            value = native_read_msr(MSR_NHM_PLATFORM_INFO); // MSR_PLATFORM_INFO
            pmin = (value >> 40) & 0xFF;
            pmax = (value >> 8) & 0xFF;

            value = native_read_msr(MSR_NHM_TURBO_RATIO_LIMIT);  // XXX this MSR is writable
            pturbo = ((value) & 255);
            TRB_DBG("Turbo pstate %u-%u (turbo:%u)\n", pmin, pmax, pturbo);

            value = native_read_msr(MSR_IA32_PERF_STATUS);
            TRB_DBG("Turbo current pstate %Lu\n", (value >> 8) & 0xFF);

            /* Set frequency to minimal on all CPUs. */
            /* FIXME: not sure how to find the compatible p-states */
            /* This clears the disengage bit for IDA */
            for_each_possible_cpu(cpu) {
                wrmsr_on_cpu(cpu, MSR_IA32_PERF_CTL, (pmin & 0xFF) << 8, 0);
            }
            break;
        case X86_VENDOR_AMD:
            TRB_DBG("Turbo vendor AMD");
            is_intel = 0;
            break;
        default:
            TRB_DBG("Turbo vendor not supported\n");
            ret = -ENODEV;
    }

exit:
    if (ret < 0)
        trb_cleanup();
    return ret;
}

MODULE_LICENSE("GPL");
module_init(trb_init);
module_exit(trb_cleanup);

MODULE_DESCRIPTION("TURBO - P-state configuration driver");
MODULE_AUTHOR("Jons-Tobias Wamhoff <m@jons.de>");
MODULE_LICENSE("GPL");

