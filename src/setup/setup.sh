#!/bin/bash

# Copyright (c) 2014 Jons-Tobias Wamhoff

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


function print_help() {
    echo "Available options:"
    echo " disable_os   Disable OS frequency scaling"
    echo " enable_os    Enable OS frequency scaling"
    echo " print_config Show configuration"
    echo " set_governor ACPI driver only (performance,powersave,ondemand,userspace)"
    echo " stop_cron    Stop all cron jobs"
    exit
}

# https://www.kernel.org/doc/Documentation/kernel-parameters.txt
CMDLINE="GRUB_CMDLINE_LINUX_DEFAULT"
CMDLINE_MANUAL="$CMDLINE=\"quiet splash notsc clocksource=hpet processor.max_cstate=0 idle=poll intel_idle.max_cstate=0 intel_pstate=disable\""
CMDLINE_DEFAULT="$CMDLINE=\"quiet splash\""
GRUBFILE="/etc/default/grub"

function set_kernel_params() {
    local CMDLINE_ARG="$1"

    echo "Backing up $GRUBFILE to $GRUBFILE.trb-old"
    cp $GRUBFILE $GRUBFILE.trb-old

    echo "Commenting all $CMDLINE in $GRUBFILE"
    sed -i "s/^$CMDLINE/#$CMDLINE/g" $GRUBFILE

    if grep --quiet "$CMDLINE_ARG" $GRUBFILE; then
        echo "Uncommenting $CMDLINE_ARG in $GRUBFILE"
        sed -i "s/#$CMDLINE_ARG/$CMDLINE_ARG/g" $GRUBFILE
    else
        echo "Adding $CMDLINE_ARG to $GRUBFILE"
        sed -i "/^GRUB_CMDLINE_LINUX=/i$CMDLINE_ARG" $GRUBFILE
    fi

    update-grub

    echo "Reboot so that changes take effect?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) reboot;;
            No ) exit;;
        esac
    done
}

if [ "$UID" -ne 0 ]; then
    echo "Please run as root"
    exit
fi

if [ -z $1 ] ; then
    echo "No option specified"
    print_help
fi

if [ $1 = "disable_os" ] ; then
    echo "Disabling OS frequency scaling"
    set_kernel_params "$CMDLINE_MANUAL"
elif [ $1 = "enable_os" ] ; then
    echo "Enabling OS frequency scaling"
    set_kernel_params "$CMDLINE_DEFAULT"
elif [ $1 = "print_config" ] ; then
    echo "Printing configuration"
    echo "/proc/cmdline:"
    cat /proc/cmdline
    echo "/sys/devices/system/cpu/cpuidle/current_driver:"
    cat /sys/devices/system/cpu/cpuidle/current_driver
    cpupower frequency-info
elif [ $1 = "set_governor" ] ; then
    if [ -z $2 ] ; then
        echo "No governor specified"
        print_help
    fi
    echo "Setting governor to $2"
    cpupower frequency-set -g $2
elif [ $1 = "stop_cron" ] ; then
    echo "Stopping all cron jobs"
    service cron stop
else
    echo "Unknown option specified"
    print_help
fi

